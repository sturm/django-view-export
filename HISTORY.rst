Release History
---------------

1.0.1 (2021-09-29)
++++++++++++++++++

 - Switch to setup.cfg.


1.0.0 (2021-09-28)
++++++++++++++++++

 - Upgrade for Python 3.6 and Django 3.2.


0.7.1 (2017-02-07)
++++++++++++++++++

 - Remove ``patterns`` URLconf function per deprecation in Django 1.8.
 - Fix example URLs in README.
 - Provide example view with underscores.
 - Document staff-only access and LOGIN_URL setting.


0.6.2 (2015-09-04)
++++++++++++++++++

**Bugfixes**

 - Fix installation error due to HISTORY.rst not being present in source.


0.6.1 (2015-08-25)
++++++++++++++++++

**Improvements**

 - Switch to Python 3 only, factor out CSV and report filename generation.


0.5.5 (2015-08-25)
++++++++++++++++++

**Bugfixes**

 - Fix installation error due to HISTORY.rst not being present in source.


0.5.4 (2015-08-09)
++++++++++++++++++

**Bugfixes**

 - Fix SQL injection vulnerability relating to "view" argument.


0.5.3 (2015-08-05)
++++++++++++++++++

**Improvements** 

 - Update documentation.
 - Rename PyPI package to ``django-view-export``.
